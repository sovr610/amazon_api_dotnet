﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace amazonAPI
{
    class AmazonResponseXML
    {

        public string parse_data(string data, string node)
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(data);
            XmlNodeList nodeList = xmlDoc.DocumentElement.SelectNodes("/Table/Product");
            string proID = "", proName = "", price = "";
            foreach (XmlNode node in nodeList)
            {
                proID = node.SelectSingleNode("Product_id").InnerText;
                proName = node.SelectSingleNode("Product_name").InnerText;
                price = node.SelectSingleNode("Product_price").InnerText;
                MessageBox.Show(proID + " " + proName + " " + price);
            }
        }
    }
}
