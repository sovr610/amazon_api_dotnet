﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace amazonAPI
{
    class Commands
    {
        private string signature_request;
        private string AWSAccessKeyId;
        private string AssociateTag;
        private string keywords;
        private string SearchIndex;

        private string cartId;
        private string HMAC;
        private string offerListingID;
        private string Quantity;

        private string _timestamp;

        private string _itemID;
        private string _condition;


        private string _itemSearch;
        private string _itemLookup;
        private string _itemSimarityLookup;
        private List<string> _itemSimlarityLookupList = new List<string>();
        private string _cartCommand;
        private string _CartClearCommand;
        private string _CartCreateCommand;
        private string _CartGetCommand;
        private string _CartModifyCommand;


        public string getItemSearch()
        {
            _itemSearch = "http://webservices.amazon.com/onca/xml?Service=AWSECommerceService&AWSAccessKeyId=" + AWSAccessKeyId + "&AssociateTag=" + AssociateTag + "&Operation=ItemSearch&Keywords=" + keywords.Replace(" ", "%20") + "&SearchIndex=" + SearchIndex + "&Timestamp=" + _timestamp + "&Signature=" + signature_request;
            return _itemSearch;

        }

        public string getitemLookupCommand()
        {
            _itemLookup = "http://webservices.amazon.com/onca/xml?Service=AWSECommerceService&AWSAccessKeyId=" + AWSAccessKeyId + "&AssociateTag=" + AssociateTag + "&Operation=ItemLookup&ItemId=" + _itemID + "&IdType=ASIN&ResponseGroup=OfferFull&Condition=" + _condition + "&Timestamp=" + signature_request + "&Signature=" + signature_request;
            return _itemLookup;
        }

        public string getSimarityLookup()
        {
            string finalItemLookup = "";
            foreach (string _item in _itemSimlarityLookupList)
            {
                finalItemLookup += _item + ",";
            }
            _itemSimarityLookup = "http://webservices.amazon.com/onca/xml?Service=AWSECommerceService&AWSAccessKeyId=" + AWSAccessKeyId + "&AssociateTag=" + AssociateTag + "&Operation=SimilarityLookup&ItemId=" + finalItemLookup + "&Timestamp=" + _timestamp + "&Signature=" + signature_request;
            return _itemSimarityLookup;
        }

        public string CartAddCommand()
        {
            _cartCommand = "http://webservices.amazon.com/onca/xml?Service=AWSECommerceService&AWSAccessKeyId=" + AWSAccessKeyId + "&AssociateTag=" + AssociateTag + "&CartId=" + cartId + "&HMAC=" + HMAC + "&Operation=CartAdd&Item.1.OfferListingId=" + offerListingID + "&Item.1.Quantity=" + Quantity + "&Timestamp=" + _timestamp + "&Signature=" + signature_request;
            return _cartCommand;
        }

        public string clearCart()
        {
            _CartClearCommand = "http://webservices.amazon.com/onca/xml?Service=AWSECommerceService&AWSAccessKeyId=" + AWSAccessKeyId + "&AssociateTag=" + AssociateTag + "&Operation=CartClear&CartId=" + cartId + "&HMAC=" + HMAC + "&Timestamp=" + _timestamp + "&Signature=" + signature_request;
            return _CartClearCommand;
        }



        public string createCart(CartItem[] itms)
        {
            _CartCreateCommand = "http:// webservices.amazon.com/onca/xml?Service=AWSECommerceService&AWSAccessKeyId=" + AWSAccessKeyId + "&AssociateTag=" + AssociateTag + "&Operation=CartCreate&";
            int x = 0;
            foreach (CartItem itmss in itms)
            {
                itmss.itemNumber = Convert.ToString(x);
                _CartCreateCommand += itmss.parseData();
            }

            _CartCreateCommand += "&Timestamp=" + _timestamp;
            _CartCreateCommand = "&Signature=" + signature_request;
            return _CartCreateCommand;
        }

        public string getCartItem()
        {
            _CartGetCommand = "http://webservices.amazon.com/onca/xml?Service=AWSECommerceService&AWSAccessKeyId=" + AWSAccessKeyId + "&AssociateTag=" + AssociateTag + "&Operation=CartGet&CartId=" + cartId + "&HMAC=" + HMAC;
            return _CartGetCommand;
        }

        





    }

    class CartItem
    {
        public string asin;
        public string Quantity;
        public string itemNumber;
        public string parseData()
        {
            string result = "Item." + itemNumber + ".ASIN=" + asin + "&";
            result += "Item." + itemNumber + ".Quantity=" + Quantity;
            return result;
        }
    }
    }

